import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    anchors: {
      a1: { x: 0, y: 0 },
      a2: { x: 0, y: 2000 },
      a3: { x: 2000, y: 2000 },
      a4: { x: 2000, y: 0 }
    }
  },
  mutations: {
    setAnchors (state, anchors) {
      state.anchors = anchors
    }
  },
  getters: {
    getAnchors: state => state.anchors
  },
  actions: {
  },
  modules: {
  }
})
