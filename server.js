const express = require('express')
const path = require('path')
const cors = require('cors')
const compression = require('compression')
const chalk = require('chalk')
const moment = require('moment')

const PORT = process.env.PORT || 8081

const app = express()
app.use(cors({
  origin: true,
  credentials: true
}))
app.use(compression())
const server = require('http').createServer(app)
const io = require('socket.io')(server)

setInterval(() => {
  let dummy = [
    {
      version: '1',
      tagId: '24576',
      success: true,
      timestamp: moment().valueOf(),
      data: {
        tagData: {},
        anchorData: [],
        coordinates: {
          x: Math.floor(Math.random() * 2000),
          y: Math.floor(Math.random() * 1500),
          z: 0
        }
      }
    },
    {
      version: '1',
      tagId: '24577',
      success: true,
      timestamp: moment().valueOf(),
      data: {
        tagData: {},
        anchorData: [],
        coordinates: {
          x: Math.floor(Math.random() * 2000),
          y: Math.floor(Math.random() * 1500),
          z: 0
        }
      }
    }
  ]
  io.sockets.emit('pozyxData', dummy)
}, 300)

app.use(express.static(path.join(__dirname, 'dist')))

server.listen(PORT, (err) => {
  if (err) console.log('ERROR starting server')
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), PORT, app.get('env'))
  console.log('  Press CTRL-C to stop\n')
})
